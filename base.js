"use strict"

var fs = require('fs');
var request = require('request');
var exec = require('child_process').exec;
var Q = require('q');

var helpers = {};


helpers.downloadFile = function(options, outfile) {
		var deferred = Q.defer();		
		let r = request(options);

		let file = fs.createWriteStream(outfile);

		// Pipe file to output.
		r.pipe(file);

		r.on('response', function(response) {
				if (response.statusCode != 200) {
						console.log("File not found.");
						file.close();
						fs.unlink(outfile);
						deferred.reject(new Error("File not found"));
				}
				// Continue file download => So basically do nothing.
		});

		file.on('finish', function() {
				deferred.resolve('Downloaded '+outfile);
		});

		r.on('error', function(err) {
				deferred.reject(err);
		});
		
		return deferred.promise;
}

helpers.rmdirr = function(path) {
		return helpers.callCmd('rm -r "'+path+'"');
};

helpers.chmodr = function(path, mode) {
		return helpers.callCmd('chmod -R '+mode+' "'+path+'"');
};


helpers.callCmd = function(cmd) {
		let deferred = Q.defer();

		exec(cmd, function(error, stdout, stderr) {
				if (error) {
						return deferred.reject(error);
				}

				return deferred.resolve(stdout);
		});

		return deferred.promise;
};

module.exports = helpers;
